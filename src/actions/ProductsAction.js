import {
    ADD_PRODUCT,
    ADD_PRODUCT_ERROR,
    ADD_PRODUCT_SUCCESS,
    DOWNLOAD_PRODUCTS,
    DOWNLOAD_PRODUCTS_ERROR,
    DOWNLOAD_PRODUCTS_SUCCESS
} from '../types/index';
import axiosClient from "../config/axios";
import Swal from "sweetalert2";

export function createNewProductAction(product) {
    return async (dispatch) => {
        dispatch(addProduct());

        try {
            await axiosClient.post('/products', product);
            dispatch(addProductSuccess(product));

            Swal.fire('Correcto', 'Producto agregado correctamente.', 'success')
        } catch (e) {
            dispatch(addProductError(true))

            Swal.fire({
                icon: "error",
                title: 'Existe un error',
                text: "Por favor intentalo mas tarde."
            })
        }
    }
}

export function getProductAction() {
    return async (dispatch) => {
        dispatch(getProducts())

        try {
            const resp = await axiosClient.get('/products');
            dispatch(getProductSuccess(resp.data));
        } catch (e) {
            dispatch(getProductError());
        }
    }
}

const addProduct = () => ({
    type: ADD_PRODUCT,
    payload: true
});

const addProductSuccess = (product) => ({
    type: ADD_PRODUCT_SUCCESS,
    payload: product
});

const addProductError = (status) => ({
    type: ADD_PRODUCT_ERROR,
    payload: status
});

const getProducts = () => ({
    type: DOWNLOAD_PRODUCTS,
    payload: true
});

const getProductSuccess = (products) => ({
    type: DOWNLOAD_PRODUCTS_SUCCESS,
    payload: products
});

const getProductError = () => ({
    type: DOWNLOAD_PRODUCTS_ERROR,
    payload: true
});
