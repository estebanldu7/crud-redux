import {combineReducers} from "redux";
import productReducer from './ProductsReducer';

export default combineReducers({
    products: productReducer
})
