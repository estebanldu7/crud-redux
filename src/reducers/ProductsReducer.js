import {
    ADD_PRODUCT,
    ADD_PRODUCT_ERROR,
    ADD_PRODUCT_SUCCESS,
    DOWNLOAD_PRODUCTS, DOWNLOAD_PRODUCTS_ERROR,
    DOWNLOAD_PRODUCTS_SUCCESS
} from '../types/index';

//Every reducer hast owner state
const initialState = {
    products: [],
    error: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_PRODUCT:
        case DOWNLOAD_PRODUCTS:
            return {
                ...state,
                loading: action.payload
            };
        case ADD_PRODUCT_SUCCESS:
            return {
                ...state,
                loading: false,
                products: [...state.products, action.payload]
            };

        case ADD_PRODUCT_ERROR:
        case DOWNLOAD_PRODUCTS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        case DOWNLOAD_PRODUCTS_SUCCESS:
            return {
                ...state,
                error: false,
                products: action.payload
            };

        default:
            return state;
    }
}
