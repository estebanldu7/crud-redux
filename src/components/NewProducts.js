import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";

//Redux action
import {createNewProductAction} from '../actions/ProductsAction';

const NewProducts = (history) => {

    // component state
    const [name, saveName] = useState('');
    const [price, savePrice] = useState(0);

    const dispatch = useDispatch();

    //get state from store
    const loading = useSelector((state) => state.products.loading);
    const error = useSelector((state) => state.products.error);

    //Call action from productAction
    const addProduct = (product) => dispatch(createNewProductAction(product));

    const submitNewProduct = (e) => {
        e.preventDefault();

        //form validate
        if(name.trim() === '' || price === 0){
            return
        }

        //AddProduct
        addProduct({
            name,
            price
        });

        history.push('/');
    };

    return (
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Agregar Nuevo Producto
                        </h2>

                        <form onSubmit={submitNewProduct}>
                            <div className="form-group">
                                <label htmlFor="">Nombre Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre Producto"
                                    name="name"
                                    value={name}
                                    onChange={e => saveName(e.target.value)}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="">Precio Producto</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Nombre Producto"
                                    name="price"
                                    value={price}
                                    onChange={e => savePrice(Number(e.target.value))}
                                />


                                <button type="submit"
                                        className="btn btn-primary font-weigh-bold text-uppercase d-block w-100">
                                    Agregar
                                </button>
                            </div>
                        </form>

                        {loading ? <p>Cargando....</p> : null}
                        {error ? <p>Error</p> : null}
                    </div>
                </div>
            </div>
        </div>
    )
};

export default NewProducts;
